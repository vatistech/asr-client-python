## Pre-requirements

* for each new tag, document the important changes in `CHANGELOG.md` (e.g. refactors, bug fixes, considerable improvements) and add required compatibility versions of other services if any 

## Steps

1. `cd` into the root folder and make sure the virtual environment si active
2. Install `twine`: `pip install twine`
3. Update `__tag__` in the `setup.py` and commit the change
4. On the previous commit add a tag that matches the `__tag__` in the `setup.py` and push the tag 
5. Empty the `./dist` directory
6. Create distribution: `python setup.py sdist`
7. Upload the distribution to PyPi: `twine upload dist/*` (provide the username and password of the PyPi account)